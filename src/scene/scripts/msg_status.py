# coding=UTF-8

# 状态类 可自己定义
class TurtlebotStatus:
    def __init__(self):
        self.READY = 200
        self.PREPARE = 201
        self.DELIVER = 202
        self.RETURN = 203
        self.WAITING = 204


class ArmStatus:
    def __init__(self):
        self.STANDBY = 400  # 硬件正常 可以工作
        self.READY = 401  # 制作完成 可以放置
        self.FINISH = 402  # 放置完成
        self.BUSY = 403


'''
提供对于发送给nove_base执行的目标点的状态信息
在move_base启动后会，持续发布。初始时为空消息
导航任务结束后，会持续发布上次的导航任务执行结果

//目标点尚未被处理
uint8 PENDING=0		

//目标点正在被处理		
uint8 ACTIVE=1		

//目标在开始执行后收到了取消请求，并已完成其执行			
uint8 PREEMPTED=2

//目标被成功实现
uint8 SUCCEEDED=3   

//目标在执行期间被中止，由于一些故障（失败）
uint8 ABORTED=4		

//目标尚未被处理就被拒绝，由于目标不可达或无效
uint8 REJECTED=5		

//在目标开始执行后，收到取消请求，尚未执行完成
uint8 PREEMPTING=6  

 //在目标开始执行前收到了取消请求，但action server尚未确认
uint8 RECALLING=7 

//在目标开始执行前收到了取消请求，且被成功取消
uint8 RECALLED=8   

 //action 客户端可以确认目标已经丢失，action服务端不应该再通过网络发送。
uint8 LOST=9 
'''


# 状态类 *不可修改*
class MovebaseStatus:
    def __init__(self):
        self.PENDING = 0
        self.ACTIVE = 1
        self.PREEMPTED = 2
        self.SUCCEEDED = 3
        self.ABORTED = 4
        self.REJECTED = 5
        self.PREEMPTING = 6
        self.RECALLING = 7
        self.RECALLED = 8
        self.LOST = 9


DELIVERER_STATUS = TurtlebotStatus()
ARM_STATUS = ArmStatus()
NAVIGATION_STATUS = MovebaseStatus()
