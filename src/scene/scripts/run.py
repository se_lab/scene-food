#!/usr/bin/env python2
# coding=UTF-8
import time

import rospy
import yaml
import os
import sys
import roslaunch
from flask import Flask, request, jsonify, render_template
from Planner import FoodPlanner
from flask_cors import CORS
#导入消息模块
sys.path.append('/home/zzm/turtlebot_ws/src/kobuki_msgs')
from kobuki_msgs.msg import SensorState
from kobuki_msgs.msg import AutoDockingActionFeedback
#更改当前python工作路径
os.chdir('/home/zzm/scene-food/src/scene/')


def load_pose_info(path="../params/pose_info.yaml"):
    current_path = os.path.dirname(os.path.realpath(__file__))
    path = os.path.join(current_path, path)
    print("loading yaml file:", path)

    file = open(path)
    data = file.read()
    file.close()

    pose_info = yaml.load(data, Loader=yaml.FullLoader)
    return pose_info


app = Flask(__name__)

# 支持跨域请求
CORS(app, supports_credentials=True)

with app.app_context():
    pose_dict = load_pose_info()
    deliverer_name = [""]  # ["turtlebot1"]
    arm_name = "arm"
    planner = FoodPlanner(pose_dict=pose_dict,
                          deliverer_name=deliverer_name,
                          arm_name=arm_name)


@app.route("/deliver_food")
def deliver():
    table = request.args.get("table")
    res = planner.run(table)
    return res

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/process")
def process():
    with open('scripts/log.txt') as file_obj:
        return file_obj.read()

#turtlebot充电状态
charge_state = {
        0 : 'DISCHARGING',
        2 : 'DOCKING_CHARGED',
        6 : 'DOCKING_CHARGING',
        18 : 'ADAPTER_CHARGED',
        22 : 'ADAPTER_CHARGING'
}

def get_charge_state():
    msg = rospy.wait_for_message('/mobile_base/sensors/core', SensorState, timeout = 1)
    return msg.charger

@app.route("/auto_charge")
def auto_charge():
    if (get_charge_state() == 0):
        os.system('roslaunch kobuki_auto_docking activate.launch &')
        file_obje = open('scripts/log.txt', 'w+')
        file_obje.write("正在前往充电桩...\n")
        while True:
            try:
                msg = rospy.wait_for_message('/dock_drive_action/feedback', AutoDockingActionFeedback, timeout=3)
            except Exception:
                file_obje.write("正在充电...\n")
                return "正在充电..."
            else:
                file_obje.write("[INFO] [" + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) +"] :" + msg.feedback.state + "\n")
                time.sleep(0.2)
    else:
        file_obje = open('scripts/log.txt', 'w+')
        file_obje.write("该设备无需充电！充电状态：" + charge_state[get_charge_state()] + "\n")
        file_obje.close()
        return "该设备无需充电！充电状态：" + charge_state[get_charge_state()]

if __name__ == '__main__':
    # 启动rviz节点观察导航情况
    # uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    # roslaunch.configure_logging(uuid)
    # tracking_launch = roslaunch.parent.ROSLaunchParent(
    #     uuid,
    #     ["/home/zzm/turtlebot_ws/src/turtlebot_viz/turtlebot_rviz_launchers/launch/view_navigation.launch"]
    # )
    # tracking_launch.start()
    os.system('roslaunch turtlebot_rviz_launchers view_navigation.launch &')

    #初始化节点‘scene_food’
    rospy.init_node("scene_food")

    # 启动flask
    try:
        app.run(host='0.0.0.0', port='5002', debug=False)
    except Exception as e:
        print(e.args)

