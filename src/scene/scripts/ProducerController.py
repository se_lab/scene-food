# coding=UTF-8

# 为机械臂提供基本能力接口
import time
import traceback

import rospy

from std_msgs.msg import String
from msg_status import ARM_STATUS
from Entity import Robot


class ArmController(Robot):
    def __init__(self, name, status, namespace,
                 max_retry=60):
        super(ArmController, self).__init__(name, status)
        self.namespace = "" if len(namespace) == 0 else "/%s" % namespace
        self.max_retry = max_retry

    # 查询机械臂状态
    def check_arm_status(self):
        try:
            arm_info = rospy.wait_for_message("/arm_state", String, timeout=2)
            arm_info = str(arm_info)[7: -1]  # filter message
            # print("arm status:", arm_info, type(arm_info))

            return {
                "code": 200,
                "data": arm_info
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    # 检查状态 直到机械臂满足状态
    def arm_wait(self, code="STANDBY"):
        try:
            for sec in range(self.max_retry):
                response = self.check_arm_status()
                # 异常
                if response["code"] != 200:
                    return response
                # 检查状态
                arm_info = response["data"]
                print("arm status:", arm_info, type(arm_info))
                if arm_info != code:
                    time.sleep(1)
                else:  # arm_info == code
                    return {
                        "code": 200,
                        "msg": "机械臂状态%s" % code
                    }
            return {
                "code:": 500,
                "msg": "机械臂等待超时，最后一次状态为%s" % arm_info
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    # 机械臂归位
    def recover(self):
        try:
            # 等待机械臂空闲
            response = self.arm_wait(code="FINISHED")
            if response["code"] != 200:
                return response
            self.set_status(ARM_STATUS.STANDBY)

            # 归位
            pub = rospy.Publisher("/reset_arm", String, queue_size=1)
            pub.publish("")
            time.sleep(1)
            pub.publish("Reset arm to original pose.")
            self.set_status(ARM_STATUS.BUSY)

            # 等待完成
            response = self.arm_wait(code="STANDBY")
            if response["code"] != 200:
                return response
            self.set_status(ARM_STATUS.STANDBY)

            return {
                "code": 200,
                "msg": "机械臂已归位"
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    # 制作
    def make_food(self):
        try:
            # 等待机械臂空闲
            response = self.arm_wait(code="STANDBY")
            if response["code"] != 200:
                return response
            self.set_status(ARM_STATUS.STANDBY)

            # 发布制作命令
            pub = rospy.Publisher("/make_coffee", String, queue_size=1)
            pub.publish("")  # empty msg
            time.sleep(1)
            pub.publish("Instruct arm to make coffee.")
            self.set_status(ARM_STATUS.BUSY)

            # 等待完成
            response = self.arm_wait(code="READY")
            if response["code"] != 200:
                return response
            self.set_status(ARM_STATUS.STANDBY)

            return {
                "code": 200,
                "msg": "制作完成"
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    # 将咖啡放置turtlebot上
    def place_food(self):
        try:
            # 等待机械臂空闲
            response = self.arm_wait(code="READY")
            if response["code"] != 200:
                return response
            self.set_status(ARM_STATUS.STANDBY)

            # 发布放置命令
            pub = rospy.Publisher("/put_coffee", String, queue_size=1)
            pub.publish("")
            time.sleep(1)
            pub.publish("Instruct arm to put coffee.")
            self.set_status(ARM_STATUS.BUSY)

            # 等待完成
            response = self.arm_wait(code="FINISHED")
            if response["code"] != 200:
                return response
            self.set_status(ARM_STATUS.STANDBY)

            return {
                "code": 200,
                "msg": "制作完成"
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }
