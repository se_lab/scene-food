# coding=UTF-8

# 为turtlebot提供基本能力接口
import math
import time
import traceback

import rospy
import tf

from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from actionlib_msgs.msg import GoalID, GoalStatusArray
from move_base_msgs.msg import MoveBaseActionFeedback

from msg_status import DELIVERER_STATUS, NAVIGATION_STATUS
from Entity import Robot


class TurtlebotController(Robot):
    def __init__(self, name, status, namespace=None, status_freq=1):
        super(TurtlebotController, self).__init__(name, status)
        self.status_freq = status_freq  # 检查move_base导航状态时的频率
        self.namespace = "" if len(namespace) == 0 else "/%s" % namespace

    @staticmethod
    def _get_pose_stamped(pose):
        pose_stamped = PoseStamped()
        pose_stamped.header.stamp = rospy.Time.now()
        pose_stamped.header.frame_id = "map"
        pose_stamped.pose.position.x = pose['x']
        pose_stamped.pose.position.y = pose['y']
        pose_stamped.pose.position.z = pose['z']

        quaternion = tf.transformations.quaternion_from_euler(
            pose['roll'],
            pose['pitch'],
            pose['yaw'])
        pose_stamped.pose.orientation.x = quaternion[0]
        pose_stamped.pose.orientation.y = quaternion[1]
        pose_stamped.pose.orientation.z = quaternion[2]
        pose_stamped.pose.orientation.w = quaternion[3]
        return pose_stamped

    @staticmethod
    def _get_pose_with_covariance_stamped(pose):
        pose_covar_stamped = PoseWithCovarianceStamped()
        pose_covar_stamped.header.stamp = rospy.Time.now()
        pose_covar_stamped.header.frame_id = "map"
        pose_covar_stamped.pose.pose.position.x = pose['x']
        pose_covar_stamped.pose.pose.position.y = pose['y']
        pose_covar_stamped.pose.pose.position.z = pose['z']

        quaternion = tf.transformations.quaternion_from_euler(
            pose['roll'],
            pose['pitch'],
            pose['yaw'])
        pose_covar_stamped.pose.pose.orientation.x = quaternion[0]
        pose_covar_stamped.pose.pose.orientation.y = quaternion[1]
        pose_covar_stamped.pose.pose.orientation.z = quaternion[2]
        pose_covar_stamped.pose.pose.orientation.w = quaternion[3]
        pose_covar_stamped.pose.covariance[6 * 0 + 0] = 0.5 * 0.5
        pose_covar_stamped.pose.covariance[6 * 1 + 1] = 0.5 * 0.5
        pose_covar_stamped.pose.covariance[6 * 5 + 5] = math.pi / 12.0 * math.pi / 12.0

        return pose_covar_stamped

    @staticmethod
    def _get_pose_info(pose):
        pose_info = {}
        (r, p, y) = tf.transformations.euler_from_quaternion(
            [pose.orientation.x,
             pose.orientation.y,
             pose.orientation.z,
             pose.orientation.w])
        pose_info['x'] = pose.position.x
        pose_info['y'] = pose.position.y
        pose_info['z'] = pose.position.z
        pose_info['roll'] = r
        pose_info['pitch'] = p
        pose_info['yaw'] = y

        return pose_info

    # 确认自身状态
    def recover(self):
        try:
            self.set_status(DELIVERER_STATUS.PREPARE)
            time.sleep(1)
            # TODO 业务逻辑待完善
            self.set_status(DELIVERER_STATUS.READY)

            return {
                "code": 200,
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    # 定位/导航相关
    def pub_initial_pose(self, pose):
        try:
            pose_stamped = self._get_pose_with_covariance_stamped(pose)  # 转换格式
            pub = rospy.Publisher(self.namespace + "/initialpose",
                                  PoseWithCovarianceStamped,
                                  queue_size=1)
            pub.publish(pose_stamped)
            return {
                "code": 200,
                "data": pose_stamped
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    def navigate_to(self, pose):
        try:
            # 发布导航任务
            rs = self.pub_navigation_pose(pose)
            if rs["code"] != 200:
                return rs
            pose_stamped = rs["data"]

            while True:
                # 查询导航状态
                rs = self.check_move_base_status()
                if rs["code"] != 200:
                    return rs

                # 判断状态
                if len(rs["data"]["status"]) == 0:
                    continue

                status = rs["data"]["status"][-1]
                if status == NAVIGATION_STATUS.SUCCEEDED:  # 成功
                    return {
                        "code": 200,
                        "data": {
                            "msg": "成功到达目的地",
                            "data": pose_stamped
                        }
                    }
                elif status == NAVIGATION_STATUS.PENDING or status == NAVIGATION_STATUS.ACTIVE:  # 执行中
                    # 间隔
                    time.sleep(self.status_freq)
                else:  # 未完成的情况
                    return {
                        "code": 500,
                        "data": {
                            "msg": "导航未完成",
                            "status": status
                        }
                    }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    def pub_navigation_pose(self, pose):
        try:
            pose_stamped = self._get_pose_stamped(pose)  # 转换格式
            pub = rospy.Publisher(self.namespace + "/move_base_simple/goal",
                                  PoseStamped,
                                  queue_size=1)

            pub.publish(PoseStamped())  # empty pose
            time.sleep(1)

            pub.publish(pose_stamped)

            return {
                "code": 200,
                "data": pose_stamped
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    def cancel_navigation(self):
        try:
            pub = rospy.Publisher(self.namespace + "/move_base/cancel",
                                  GoalID,
                                  queue_size=1)
            first_goal = GoalID()
            pub.publish(first_goal)

            return {
                "code": 200,
                "data": first_goal
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    # 状态信息相关
    def check_amcl_pose(self):
        try:
            amcl_pose = rospy.wait_for_message(self.namespace + "/amcl_pose",
                                               PoseWithCovarianceStamped,
                                               timeout=2)
            pose = amcl_pose.pose.pose
            pose_info = self._get_pose_info(pose)
            covariance = amcl_pose.pose.covariance

            return {
                "code": 200,
                "data": {
                    "poseInfo": pose_info,
                    "covariance": covariance
                }
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    def check_robot_pose(self):
        try:
            move_base_feedback = rospy.wait_for_message(self.namespace + "/move_base/feedback",
                                                        MoveBaseActionFeedback,
                                                        timeout=2)
            pose = move_base_feedback.feedback.base_position.pose
            pose_info = self._get_pose_info(pose)

            return {
                "code": 200,
                "data": {
                    "poseInfo": pose_info
                }
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    def check_move_base_status(self):
        try:
            move_base_status = rospy.wait_for_message(self.namespace + "/move_base/status",
                                                      GoalStatusArray,
                                                      timeout=2)
            status_list = []
            for each in move_base_status.status_list:
                status_list += [each.status]

            return {
                "code": 200,
                "data": {
                    "status": status_list
                }
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }

    def pick_up(self):  # 取餐
        try:
            time.sleep(3)
            # TODO 业务逻辑带完善

            return {
                "code": 200
            }
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            return {
                "code": 400,
                "msg": e.args
            }
