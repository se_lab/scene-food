class Robot(object):
    def __init__(self, name, status):
        self.name = name
        self.status = status

    def set_status(self, status):
        self.status = status

    def get_status(self):
        return self.status