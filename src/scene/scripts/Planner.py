# coding=UTF-8
from __future__ import print_function

import time
import traceback
import logging
import os

from flask import jsonify

from DelivererController import TurtlebotController
from ProducerController import ArmController
from msg_status import DELIVERER_STATUS, ARM_STATUS, NAVIGATION_STATUS


class FoodPlanner():
    def __init__(self, pose_dict, deliverer_name, arm_name,
                 wait_time=1, max_retry=60):
        # 初始化参数
        self.wait_time = wait_time  # 轮询查找空闲机器人时的等待时间
        self.max_retry = max_retry  # 轮询查找空闲机器人时的最大次数
        self.pose_dict = pose_dict  # 桌子信息
        # 日志相关
        log_dir = "logs/"
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(level=logging.DEBUG)
        formatter = logging.Formatter('%(levelname)s - %(asctime)s - %(name)s[line:%(lineno)d] - %(message)s')

        handler_log = logging.FileHandler(log_dir + "log_%s.txt" % time.time(), mode='w')
        handler_log.setLevel(level=logging.DEBUG)
        handler_log.setFormatter(formatter)

        handler_info = logging.FileHandler("scripts/log.txt", mode='w')
        handler_info.setLevel(level=logging.DEBUG)
        handler_info.setFormatter(formatter)

        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        console.setFormatter(formatter)
        self.logger.addHandler(handler_log)
        self.logger.addHandler(handler_info)
        self.logger.addHandler(console)

        # 暂时写死 可根据需要配置
        self.deliverer = []  # 送餐者 - turtlebot
        for name in deliverer_name:
            deliverer = TurtlebotController(name=name,
                                            status=DELIVERER_STATUS.PREPARE,
                                            namespace=name)
            deliverer.recover()  # 确认状态 确认后进入READY状态
            self.deliverer.append(deliverer)
        # 制作者 - 机械臂 （暂时只写一个 如有需要可拓展）
        self.producer = ArmController(name=arm_name,
                                      status=ARM_STATUS.STANDBY,
                                      namespace=arm_name)
        # self.producer.recover() # 确认状态
        # self.producer.set_status(ARM_STATUS.STANDBY)

    def get_table_info(self, table):
        if table not in self.pose_dict.keys():
            return {
                "code": 400,
                "msg": "无效的位置信息"
            }
        return {
            "code": 200,
            "data": self.pose_dict[table]
        }

    # 返回首个处于ready状态的turtlebot 每wait_time秒遍历一次 最多尝试max_retry次
    def find_deliverer(self):
        for sec in range(self.max_retry):
            for each in self.deliverer:  # 遍历每个turtlebot
                if each.get_status() == DELIVERER_STATUS.READY:
                    # 检查move_base状态
                    rs = each.check_move_base_status()
                    if rs["code"] != 200:   # 异常
                        return rs

                    return {
                        "code": 200,
                        "data": each,
                    }
            time.sleep(self.wait_time)
        return {    # 无空闲
            "code": 200,
            "data": None
        }

    # 等待机械臂
    def wait_for_arm(self):
        for sec in range(self.max_retry):
            if self.producer.get_status() == ARM_STATUS.STANDBY:
                return True
            time.sleep(self.wait_time)
        return False  # 未完成

    # 完整场景业务
    def run(self, table):
        try:
            # 检查目标位置
            rs = self.get_table_info(table)
            if rs["code"] != 200:
                self.logger.error("出现异常：" + str(rs["msg"]))
                return jsonify(rs)
            pose = rs["data"]

            # 查找闲置deliverer
            self.logger.info("正在寻找空闲的送餐机器人...")
            time.sleep(0.5)
            rs = self.find_deliverer()
            if rs["code"] != 200:
                self.logger.error("出现异常：" + str(rs["msg"]))
                return jsonify(rs)
            deliverer = rs["data"]
            if deliverer is None:
                self.logger.warning("暂未找到空闲的送餐机器人")
                return jsonify({
                    "code": 0,
                    "msg": "暂未找到空闲的送餐机器人"
                })
            self.logger.info("找到空闲送餐机器人" + str(deliverer.name))

            # 发布deliverer初始位置
            self.logger.info("正在发布" + str(deliverer.name) + "初始位置...")
            time.sleep(0.5)
            rs = self.get_table_info("start")  # 获取起点位置
            if rs["code"] != 200:
                self.logger.error("出现异常：" + str(rs["msg"]))
                return jsonify(rs)
            pose_start = rs["data"]
            rs = deliverer.pub_initial_pose(pose_start)
            time.sleep(10)
            rs = deliverer.pub_initial_pose(pose_start)
            time.sleep(10)
            if rs["code"] != 200:
                self.logger.error("出现异常：" + str(rs["msg"]))
                deliverer.recover()
                return jsonify(rs)
            self.logger.info("初始位置已发布，位置信息：" + str(rs["data"]))
            
            # 等待arm完成手头动作
            self.logger.info("正在等待机械臂...")
            time.sleep(0.5)
            if not self.wait_for_arm():
                self.logger.warning("机械臂繁忙，暂时不可用")
                return jsonify({
                    "code": 0,
                    "msg": "机械臂繁忙"
                })
            self.logger.info("机械臂可用")

            # deliverer收到任务开始等待 arm开始制作
            self.logger.info("机械臂制作中...")
            time.sleep(0.5)
            deliverer.set_status(DELIVERER_STATUS.WAITING)
            self.producer.set_status(ARM_STATUS.BUSY)
            rs = self.producer.make_food()
            if rs["code"] != 200:  # 异常情况
                self.logger.error("出现异常：" + str(rs["msg"]))
                return jsonify(rs)
            self.logger.info("机械臂制作完成")

            # arm进行放置操作
            self.logger.info("机械臂放置中...")
            time.sleep(0.5)
            self.producer.set_status(ARM_STATUS.BUSY)
            rs = self.producer.place_food()
            if rs["code"] != 200:
                self.logger.error("出现异常：" + str(rs["msg"]))
                return jsonify(rs)
            self.logger.info("机械臂放置完成")

            # arm归位
            self.logger.info("机械臂归位中...")
            time.sleep(0.5)
            rs = self.producer.recover()
            if rs["code"] != 200:
                self.logger.error("出现异常：" + str(rs["msg"]))
                return jsonify(rs)
            self.logger.info("机械臂归位完成")
            

            # deliverer开始送餐
            self.logger.info("开始派送，导航中...")
            time.sleep(0.5)
            deliverer.set_status(DELIVERER_STATUS.DELIVER)
            rs = deliverer.navigate_to(pose)
            if rs["code"] == 200:
                self.logger.info("派送完成" + str(rs["data"]))
            elif rs["code"] == 500:
                self.logger.error("导航未完成 状态代码：" + str(rs["data"]["status"]))
                deliverer.recover()
                return jsonify(rs)
            else:
                self.logger.error("出现异常：" + str(rs["msg"]))
                deliverer.recover()
                return jsonify(rs)

            # 等待取餐
            self.logger.info("等待取餐...")
            time.sleep(0.5)
            deliverer.set_status(DELIVERER_STATUS.WAITING)
            rs = deliverer.pick_up()
            if rs["code"] != 200:
                self.logger.error("出现异常：" + str(rs["msg"]))
                deliverer.recover()
                return jsonify(rs)
            self.logger.info("取餐完成")

            # 返回
            self.logger.info("开始返回，导航中...")
            time.sleep(0.5)
            deliverer.set_status(DELIVERER_STATUS.RETURN)
            rs = deliverer.navigate_to(pose_start)
            if rs["code"] != 200:
                self.logger.error("出现异常：" + str(rs["data"]))
                deliverer.recover()
                return jsonify(rs)
            self.logger.info("返回完成" + str(rs["data"]))

            # 重新确认状态
            self.logger.info("确认%s状态" % deliverer.name)
            time.sleep(0.5)
            deliverer.recover()
            self.logger.info("%s准备就绪" % deliverer.name)

            self.logger.info("本次任务顺利完成")
            return jsonify({
                "code": 200,
                "msg": "本次任务顺利完成"
            })
        except Exception as e:
            print(e.args)
            print(traceback.format_exc())
            self.logger.error("出现异常：" + str(e.args))
            return jsonify({
                "code": 400,
                "msg": e.args
            })
