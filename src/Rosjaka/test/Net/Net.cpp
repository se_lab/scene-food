#include "Net.h"
#include "WebSocketClient.h"
#include "ShareData.h"
#include <iostream>
#include "json.hpp"
#include "common.h"
#include "thread"

string genSendMsg(string op,string topic,string type)
{
    nlohmann::json j;
    j["op"] = op;
    j["topic"] = topic;
    j["type"] = type;
    auto jarray = nlohmann::json::array();
    jarray.emplace_back(j);
    auto str = jarray.dump();
    str.erase(str.find_first_of("["),1);
    str.erase(str.find_last_of("]"),1);
    return str;
}

void setWsclient()
{
using namespace hv;
const char* url = "ws://localhost:9090";

WebSocketClient ws;
ws.onopen = [&ws]() {
    string str = genSendMsg("advertise","/arm_state","std_msgs/String");
    // nlohmann::json j;
    // j["op"] = "advertise";
    // j["topic"] = "/arm_state";
    // j["type"] = "std_msgs/String";
    // auto jarray = nlohmann::json::array();
    // jarray.emplace_back(j);
    // auto str = jarray.dump();
    // str.erase(str.find_first_of("["),1);
    // str.erase(str.find_last_of("]"),1);
    ws.send(str);

    // nlohmann::json s;
    // s["op"] = "subscribe";
    // s["topic"] = "/make_coffee";
    // auto jarray2 = nlohmann::json::array();
    // jarray2.emplace_back(s);
    // auto str2 = jarray2.dump();
    // str2.erase(str2.find_first_of("["),1);
    // str2.erase(str2.find_last_of("]"),1);
    // ws.send(str2);
    str = genSendMsg("subscribe","/make_coffee","std_msgs/String");
    ws.send(str);

    str = genSendMsg("subscribe","/put_coffee","std_msgs/String");
    ws.send(str);

    str = genSendMsg("subscribe","/reset_arm","std_msgs/String");
    ws.send(str);
    };
    ws.onclose = []() {
        printf("onclose\n");
    };
    
    ws.onmessage = [](const std::string& msg) {
       
        auto jmsg = nlohmann::json::parse(msg.c_str());
        string msg_topic = jmsg["topic"].get<std::string>();
        if(msg_topic == "/put_coffee")  //placeCoff
        {
            CShareData::GetInstance()->SetSig(SIGPLACECOF);
        }
        if(msg_topic == "/make_coffee")  //makeCoff
        {
            CShareData::GetInstance()->SetSig(SIGMKCOF);
        }
        if(msg_topic == "/reset_arm")  //makeCoff
        {
            CShareData::GetInstance()->SetSig(SIGRESET);
        }
        printf("onmessage: %s\n", msg.c_str());
        printf("%s\n",msg_topic.c_str());
    };

    // reconnect: 1,2,4,8,10,10,10...
    reconn_setting_t reconn;
    reconn_setting_init(&reconn);
    reconn.min_delay = 1000;
    reconn.max_delay = 10000;
    reconn.delay_policy = 2;
    ws.setReconnect(&reconn);

    ws.open(url);
    hv_delay(1000);
    
    while (1) 
    {
      E_STATE s =  CShareData::GetCurState();
        std::string str_state;
        switch (s) {
        case STANDBY:
            str_state = "STANDBY";
            break;
        case BUSY:
            str_state = "BUSY";
            break;
        case READY:
            str_state = "READY";
            break;
        case FINISHED:
            str_state = "FINISHED";
            break;
        default:
            break;
        }
    nlohmann::json j_state;
    j_state["op"] = "publish";
    j_state["topic"] = "/arm_state";
    nlohmann::json content;
    content["data"] = str_state;
    j_state["msg"] = content;
    auto jarray = nlohmann::json::array();
    jarray.emplace_back(j_state);
    auto netmsg = jarray.dump();
    netmsg.erase(netmsg.find_first_of("["),1);
    netmsg.erase(netmsg.find_last_of("]"),1);
    ws.send(netmsg);

    hv_delay(1000);
    }
}

CNet::CNet()
{
    printf("GEN CNET\n");
    //setRouter();
    std::thread wsThread(setWsclient);
    wsThread.detach();
    sleep(1);
    // hv::hv_delay(1000);
    //setWsclient();
    printf("DO SETROUTER\n");
    // m_pSubMkc = new WebSocketClient();
    // m_pSubMkc->onmessage[](){

    // };
}

void CNet::setRouter()
{

    printf("setRouter\n");
    m_pRouter.GET("/checkArmState", [](HttpRequest* req, HttpResponse* resp) {
        E_STATE s =  CShareData::GetCurState();
        std::string str;
        switch (s) {
        case STANDBY:
            str = "STANDBY";
            break;
        case BUSY:
            str = "BUSY";
            break;
        case READY:
            str = "READY";
            break;
        case FINISHED:
            str = "FINISHED";
            break;
        default:
            break;
        }
        return resp->String(str);
        });

    m_pRouter.GET("/pubMakeCoffee", [](HttpRequest* req, HttpResponse* resp)
    {
        CShareData::GetInstance()->SetSig(SIGMKCOF);
        return resp->String("0");
    });


    m_pRouter.GET("/pubPlaceCoffee", [](HttpRequest* req, HttpResponse* resp)
    {
        CShareData::GetInstance()->SetSig(SIGPLACECOF);
        return resp->String("0");
    });

    m_qServer.port = 8080;
    m_qServer.service = &m_pRouter;
    http_server_run(&m_qServer);

    printf("end setRouter\n");

}

// void CNet::setWsclient()
// {


// using namespace hv;
// const char* url = "ws://localhost:9090";

// WebSocketClient ws;
// ws.onopen = [&ws]() {
//     nlohmann::json j;
//     j["op"] = "advertise";
//     j["topic"] = "/armState";
//     j["type"] = "std_msgs/String";
//     auto jarray = nlohmann::json::array();
//     jarray.emplace_back(j);
//     auto str = jarray.dump();
//     str.erase(str.find_first_of("["),1);
//     str.erase(str.find_last_of("]"),1);
//     ws.send(str);

//     nlohmann::json s;
//     s["op"] = "subscribe";
//     s["topic"] = "/armState";
//     auto jarray2 = nlohmann::json::array();
//     jarray2.emplace_back(s);
//     auto str2 = jarray2.dump();
//     str2.erase(str2.find_first_of("["),1);
//     str2.erase(str2.find_last_of("]"),1);
//     ws.send(str2);
//     };
//     ws.onclose = []() {
//         printf("onclose\n");
//     };
    
//     ws.onmessage = [](const std::string& msg) {
       
//         auto jmsg = nlohmann::json::parse(msg.c_str());
//         string msg_topic = jmsg["topic"].get<std::string>();
//         if(msg_topic == "/placeCoff")
//         {
//             CShareData::GetInstance()->SetSig(SIGPLACECOF);
//         }
//         if(msg_topic == "/makeCoff")
//         {
//             CShareData::GetInstance()->SetSig(SIGMKCOF);
//         }
//         printf("onmessage: %s\n", msg.c_str());
//         printf("%s\n",msg_topic.c_str());
//     };

//     // reconnect: 1,2,4,8,10,10,10...
//     ReconnectInfo reconn;
//     reconn.min_delay = 1000;
//     reconn.max_delay = 10000;
//     reconn.delay_policy = 2;
//     ws.setReconnect(&reconn);

//     ws.open(url);

//     hv_delay(1000);
    
//     while (1) 
//     {
//       E_STATE s =  CShareData::GetCurState();
//         std::string str_state;
//         switch (s) {
//         case STANDBY:
//             str_state = "STANDBY";
//             break;
//         case BUSY:
//             str_state = "BUSY";
//             break;
//         case READY:
//             str_state = "READY";
//             break;
//         case FINISHED:
//             str_state = "FINISHED";
//             break;
//         default:
//             break;
//         }
//     nlohmann::json j_state;
//     j_state["op"] = "publish";
//     j_state["topic"] = "/armState";
//     nlohmann::json content;
//     content["data"] = str_state;
//     j_state["msg"] = content;
//     auto jarray = nlohmann::json::array();
//     jarray.emplace_back(j_state);
//     auto netmsg = jarray.dump();
//     netmsg.erase(netmsg.find_first_of("["),1);
//     netmsg.erase(netmsg.find_last_of("]"),1);
//     ws.send(netmsg);
//     hv_delay(1000);
//     }
// }

