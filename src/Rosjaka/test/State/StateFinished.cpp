//
// Created by 17501 on 2022/1/12.
//

#include "StateFinished.h"

bool CStateFinished::checkPreCond() {}

E_STATE CStateFinished::processSig(E_SIG sig) {
    if(sig == SIGRESET)
            return BUSY;
    else
        return FINISHED;
}
