//
// Created by 17501 on 2022/1/12.
//

#ifndef MUTIL_JAKA_STATESTANDBY_H
#define MUTIL_JAKA_STATESTANDBY_H
#include "StateBase.h"
class CStateStandBy : public CStateBase
{
public:
    CStateStandBy();
    virtual E_STATE processSig(E_SIG sig);
    ~CStateStandBy(){}
private:
    virtual bool checkPreCond();
};


#endif //MUTIL_JAKA_STATESTANDBY_H
