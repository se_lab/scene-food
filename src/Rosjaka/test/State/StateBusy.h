//
// Created by 17501 on 2022/1/12.
//

#ifndef MUTIL_JAKA_STATEPREPARE_H
#define MUTIL_JAKA_STATEPREPARE_H

#include "StateBase.h"

class CStateBusy: public CStateBase {
public:
    CStateBusy(){}
    virtual E_STATE processSig(E_SIG sig);
    ~CStateBusy(){}
private:
    virtual bool checkPreCond();
};


#endif //MUTIL_JAKA_STATEPREPARE_H
