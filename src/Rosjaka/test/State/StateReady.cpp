//
// Created by 17501 on 2022/1/12.
//

#include "StateReady.h"

CStateReady::CStateReady() {

}

bool CStateReady::checkPreCond() {}

E_STATE CStateReady::processSig(E_SIG sig) {
    if(checkPreCond())
        if(sig == SIGPLACECOF)
            return BUSY;
    else
        return READY;
}
