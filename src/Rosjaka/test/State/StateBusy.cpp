//
// Created by 17501 on 2022/1/12.
//

#include "StateBusy.h"

bool CStateBusy::checkPreCond() {}

E_STATE CStateBusy::processSig(E_SIG sig) {
    E_STATE last_state = CShareData::GetInstance()->GetLastState();
    if(last_state == STANDBY)
    {
        if(sig == SIGFINMOVE)
            return READY;
        else
            return STANDBY;
    }
    else if(last_state == READY)
    {
        if(sig == SIGFINMOVE)
            return FINISHED;
        else
            return READY;
    }
    else if(last_state == FINISHED)
    {
        if(sig == SIGFINMOVE)
            return STANDBY;
        else
            return FINISHED;
    }

}

