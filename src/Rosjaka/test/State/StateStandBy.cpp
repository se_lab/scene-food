//
// Created by 17501 on 2022/1/12.
//

#include "StateStandBy.h"
#include "stdio.h"
CStateStandBy::CStateStandBy() {}

bool CStateStandBy::checkPreCond() {
    bool isEnable = CShareData::IsEnable();
    bool isPower = CShareData::IsPower();
    return (isEnable && isPower);
}

E_STATE CStateStandBy::processSig(E_SIG sig) {
    if(checkPreCond())
        // 实际处理的过程
        if(sig == SIGMKCOF)
            return BUSY;
    else
        return STANDBY;
}

