//
// Created by 17501 on 2022/1/12.
//

#ifndef MUTIL_JAKA_STATEREADY_H
#define MUTIL_JAKA_STATEREADY_H

#include "StateBase.h"
class CStateReady : public CStateBase
{
public:
    CStateReady();
    virtual E_STATE processSig(E_SIG sig);
    ~CStateReady(){}
private:
    virtual bool checkPreCond();

};


#endif //MUTIL_JAKA_STATEREADY_H
