//
// Created by 17501 on 2022/1/12.
//

#ifndef MUTIL_JAKA_STATEFINISHED_H
#define MUTIL_JAKA_STATEFINISHED_H

#include "StateBase.h"
class CStateFinished : public CStateBase{
public:
    CStateFinished(){}
    virtual E_STATE processSig(E_SIG sig);
    ~CStateFinished(){}
private:
    virtual bool checkPreCond();
};


#endif //MUTIL_JAKA_STATEFINISHED_H
