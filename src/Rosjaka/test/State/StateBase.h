//
// Created by 17501 on 2022/1/12.
//
#include "../Common/common.h"
#include "../ShareData/ShareData.h"
#ifndef MUTIL_JAKA_STATE_BASE_H
#define MUTIL_JAKA_STATE_BASE_H
class CStateBase
{
public:
    CStateBase(){}
    virtual E_STATE processSig(E_SIG sig)=0;
    virtual ~CStateBase() = 0;
private:
    virtual bool checkPreCond()=0;

};

#endif //MUTIL_JAKA_STATE_BASE_H
