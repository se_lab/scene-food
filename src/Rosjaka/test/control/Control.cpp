//
// Created by 17501 on 2022/1/12.
//

#include "Control.h"
#include "StateBase.h"
#include "StateStandBy.h"
#include "StateReady.h"
#include "StateBusy.h"
#include "StateFinished.h"
#include "ShareData/ShareData.h"
#include "Net/Net.h"
#include "Robot/Zu7Control.h"
#include "iostream"
#include "thread"
using namespace std;
Control::Control() {
    printf("GEN CONTROL\n");
    m_pStandBy = new CStateStandBy();
    m_pBusy = new CStateBusy();
    m_pReady = new CStateReady();
    m_pFinished = new CStateFinished();
    m_pShareData = CShareData::GetInstance();
    m_pShareData->SetParent(this);
     m_pZuControl = new CZuControl();
     m_pZuControl->Inital();
    m_pZuControl->Boot();

   m_pNet = new CNet();
    printf("end GEN CONTROL\n");
}

void Control::sltExit()
{
    m_pZuControl->Shutdown();
}

void Control::handleSig(E_SIG sig) {
    E_STATE cur = CShareData::GetCurState();
    printf("control.cpp   recv sig is %d\n",sig);
    switch (cur) {
        case STANDBY:   //收到sigmakecoffee，转为Busy
            cur = m_pStandBy->processSig(sig);
            printf("last is standby,  cur is %d\n",cur);
            if(cur == BUSY)
                {
                    m_pShareData->SetCurState(cur);
                    m_pZuControl->MakeCoffee();
                }
            break;
        case READY:    //收到sigplacecoffee，转为busy
            cur = m_pReady->processSig(sig);
            cout <<"READY"<<endl;
            if(cur == BUSY)
                {
                    m_pShareData->SetCurState(cur);
                    m_pZuControl->PlaceCoffee();
                }
            break;
        case FINISHED:  //收到sigfin，转为busy
            cur = m_pFinished->processSig(sig);
            if(cur == BUSY)
                {  
                    m_pShareData->SetCurState(cur);
                    m_pZuControl->Home();
                }
            cout <<"FINISHED"<<endl;
            break;
        case BUSY:   //收到sigfin，转为下一状态
            cur = m_pBusy->processSig(sig);
             m_pShareData->SetCurState(cur);
            printf("last is busy,  cur is %d\n",cur);
            break;
        default:
            break;
    }
    printf("control .cpp  cur state is : %d\n",cur);
}


Control::~Control() {
    delete m_pStandBy;
    m_pStandBy = nullptr;
    delete m_pBusy;
    m_pBusy = nullptr;
    delete m_pReady;
    m_pReady = nullptr;
    delete m_pFinished;
    m_pFinished = nullptr;
    delete m_pZuControl;
    m_pZuControl = nullptr;
    delete m_pNet;
    m_pNet = nullptr;
    delete m_pShareData;
    m_pShareData = nullptr;
}
