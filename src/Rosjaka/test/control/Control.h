//
// Created by 17501 on 2022/1/12.
//

#include "common.h"
#include "thread"
class CStateBase;
class CStateStandBy;
class CStateReady;
class CStateBusy;
class CStateFinished;
class CShareData;
class CNet;
class CZuControl;

using namespace std;
class Control {
public:
    Control();
    ~Control();
    void sltExit();
    void handleSig(E_SIG);

private:
    CStateBase *m_pStandBy;
    CStateBase *m_pReady;
    CStateBase *m_pBusy;
    CStateBase *m_pFinished;
    CShareData *m_pShareData;
    CNet *m_pNet;
    CZuControl *m_pZuControl;
};

