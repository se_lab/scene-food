#ifndef _JHERR_H_
#define _JHERR_H_

#define ERR_SUCC                 0
#define ERR_INVALID_HANDLER		 -1
#define	ERR_INVALID_PARAMETER    -2
#define ERR_COMMUNICATION_ERR	 -3
#define ERR_KINE_INVERSE_ERR     -4
#define ERR_EMERGENCY_PRESSED    -5
#define ERR_NOT_POWERED          -6
#define ERR_NOT_ENABLED          -7
#define ERR_DISABLE_SERVOMODE    -8
#define ERR_NOT_OFF_ENABLE       -9
#define ERR_PROGRAM_IS_RUNNING   -10
#define ERR_CANNOT_OPEN_FILE     -11

#endif
