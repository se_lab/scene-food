#include "inc/JAKAZuRobot.h"
#include "inc/jakaAPI.h"
#include "inc/jkerr.h"
#include "inc/jktypes.h"
#include "../Common/common.h"
class CGrip;
class CZuControl
{
    public:
        CZuControl();
        ~CZuControl();
        void Inital();
        void MakeCoffee();
        void PlaceCoffee();
        void Shutdown();
        void Boot();
        void Home();

    private:
        void updateHardInfo(E_SIG);
        void updateHardInfo();
        JAKAZuRobot *m_pRobot;
        JointValue m_jhomePos;
        CGrip *m_pGripper;
};