#include "Zu7Control.h"
#include "../ShareData/ShareData.h"
#include <unistd.h>
#include "../Common/common.h"
#include "../Grip/grip.h"
#include "../Opentracing/trace.h"
const char * JAKAIP = "192.168.31.156";
const char * GRIPPERIP = "192.168.31.29";
const int GRIPPERPORT = 8888;
double angle2rad(double angle)
{
    return angle / 57.3;
}
CZuControl::CZuControl()
{
    printf("GEN zuControl \n");
     m_jhomePos={angle2rad(-42),angle2rad(5),angle2rad(144),angle2rad(100),angle2rad(11),angle2rad(28)};
    
}

CZuControl::~CZuControl()
{
    if(m_pRobot != NULL)
    {
            Shutdown();
            m_pRobot->login_out();
    }
    CTrace& trace_manger = CTrace::GetInstance();
    trace_manger.Close("ZuControl");
}

void CZuControl::Inital()
{
    CTrace& trace_manger = CTrace::GetInstance();
    trace_manger.GenTracer("ZuControl");
    trace_manger.StartSpan("ZuControl","loginSp");
    trace_manger.GenNewContext("ZuControl","loginSp","initCtx");
    m_pRobot = new JAKAZuRobot();
   if(m_pRobot != NULL)
    {
        error_t ret =  m_pRobot->login_in(JAKAIP);
        if(ret == ERR_SUCC)
            printf("robot  login SUCC\n");    
         else
            printf("robot login FAILED\n");
        trace_manger.SetTag("loginSp","login result",ret);
    }
    // m_pGripper = new CGrip();
    // m_pGripper->InitialGripper();
    trace_manger.Finish("loginSp");
}

void CZuControl::MakeCoffee()
{
    //m_pRobot->linear_move();
    CTrace& trace_manger = CTrace::GetInstance();
    trace_manger.StartSpan("ZuControl","makeCofSp","initCtx",CHILDOF);
    printf("czucontrol.cpp  make coffee start\n");
    JointValue  pot={angle2rad(-29),angle2rad(139),angle2rad(109),angle2rad(-67),angle2rad(-106),angle2rad(135)};
    m_pRobot->joint_move( &pot, ABS ,true ,1 ); //绝对运动
    trace_manger.SetTag("makeCofSp","joint 1",angle2rad(-29));
    trace_manger.SetTag("makeCofSp","joint 2",angle2rad(139));
    trace_manger.SetTag("makeCofSp","joint 3",angle2rad(109));
    trace_manger.SetTag("makeCofSp","joint 4",angle2rad(-67));
    trace_manger.SetTag("makeCofSp","joint 5",angle2rad(-106));
    trace_manger.SetTag("makeCofSp","joint 6",angle2rad(135));
    trace_manger.SetTag("makeCofSp","Movement mode ","ABS");
    // m_pGripper->Grip();
    printf("czucontrol.cpp  make coffee end\n");
    updateHardInfo(SIGFINMOVE);

}

void CZuControl::PlaceCoffee()
{
    JointValue  pot={angle2rad(70),angle2rad(0),angle2rad(0),angle2rad(0),angle2rad(0),angle2rad(0)};
    m_pRobot->joint_move( &pot, INCR ,true ,1 ); //绝对运动
    //...精确位置
    pot={angle2rad(39),angle2rad(173),angle2rad(90),angle2rad(-69),angle2rad(-87),angle2rad(125)};
    m_pRobot->joint_move( &pot, ABS ,true ,1 ); //绝对运动
    sleep(2);
    pot={angle2rad(42),angle2rad(118),angle2rad(90),angle2rad(-30),angle2rad(-87),angle2rad(125)};
    m_pRobot->joint_move( &pot, ABS ,true ,1 ); //绝对运动

    // m_pGripper->Open();
    //...发送完成信号需要从小车得到确认信号
    updateHardInfo(SIGFINMOVE);
    sleep(2);
    //m_pRobot->linear_move();
    //Home();//或者移动到其他点
}

void CZuControl::Boot()
{
    CTrace& trace_manger = CTrace::GetInstance();
    trace_manger.StartSpan("ZuControl","BootSp","initCtx",CHILDOF);
    error_t ret = 1;
    ret = m_pRobot->power_on();
    trace_manger.SetTag("BootSp","power_on result",ret);
    ret = m_pRobot->enable_robot();
    trace_manger.SetTag("BootSp","enable result",ret);

    // while(ret != ERR_SUCC)
    // {
    //     ret = m_pRobot->power_on();
    //     printf("POWERING------------\n");
    // }
    // printf("POWER ON SUCC\n");
    // ret = 1;
    // while(ret != ERR_SUCC)
    // {
    //     ret = m_pRobot->enable_robot();
    //     printf("ENABLING------------\n");
    //     printf("ERROR ID is : %d\n",ret);
    // }
    // printf("ENABLE SUCC\n");
    updateHardInfo();
    Home();     //开机归位，信号是finish，state开机时是standby，不会处理finish信号。所以没有问题。
    trace_manger.Finish("BootSp");
}

void CZuControl::Shutdown()
{ 
    CTrace& trace_manger = CTrace::GetInstance();
    trace_manger.StartSpan("ZuControl","ShutdownSp","initCtx",CHILDOF);
    error_t ret = 1;
    ret = m_pRobot->disable_robot();
    trace_manger.SetTag("ShutdownSp","disable_robot result",ret);
    ret = m_pRobot->power_off();
    trace_manger.SetTag("ShutdownSp","power_off result",ret);
    updateHardInfo();
}

void CZuControl::Home()
{
    m_pRobot->joint_move(&m_jhomePos,ABS,TRUE,1);
    updateHardInfo(SIGFINMOVE);

}

void CZuControl::updateHardInfo(E_SIG sig)
{
    CShareData::GetInstance()->SetSig(sig);
    printf("zucontrol.cpp  send finish sig\n");
    updateHardInfo();
}

void CZuControl::updateHardInfo()
{
    RobotState state = {false,false,false};
    m_pRobot->get_robot_state(&state);
    CShareData::GetInstance()->SetEnableMsg(state.servoEnabled);
    CShareData::GetInstance()->SetEnableMsg(state.poweredOn);
    printf("zucontrol::updateHardinfo Enable: %d Power : %d\n",state.servoEnabled,state.poweredOn);
}