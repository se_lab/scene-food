//
// Created by 17501 on 2022/1/12.
//

#ifndef MUTIL_JAKA_SHAREDATA_H
#define MUTIL_JAKA_SHAREDATA_H

#include "../Common/common.h"
#include "queue"

class Control;

class CShareData {
public:
    //要改成单例模式

    static bool IsPower();

    static bool IsEnable();

    static E_STATE GetCurState();

    static E_STATE GetLastState();

    static CShareData* GetInstance();

    void SetParent(Control *parent);
    
    void SetCurState(E_STATE s);

    void SetPowerMsg(bool power_msg);

    void SetEnableMsg(bool enable_msg);
            
    void SetSig(E_SIG);
private:


    CShareData();
    
    static bool m_bPowerMsg;
    static bool m_bEnableMsg;
    static E_STATE m_eCurState;
    static E_STATE m_eLastState;

    Control *m_pParent;
};


#endif //MUTIL_JAKA_SHAREDATA_H
