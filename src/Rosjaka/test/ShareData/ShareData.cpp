//
// Created by 17501 on 2022/1/12.
//

#include "ShareData.h"
#include "Control.h"

bool CShareData::m_bEnableMsg = true;
bool CShareData::m_bPowerMsg = true;
E_STATE CShareData::m_eCurState = STANDBY;
E_STATE CShareData::m_eLastState = STANDBY;
CShareData::CShareData()
{
}

bool CShareData::IsPower()  {
    return m_bPowerMsg;
}

bool CShareData::IsEnable()  {
    return m_bEnableMsg;
}

E_STATE CShareData::GetCurState()
{
    return m_eCurState;
}

E_STATE CShareData::GetLastState()
{
    return m_eLastState;
}

CShareData* CShareData::GetInstance()
{
    static CShareData instance;
    return &instance;
}

void CShareData::SetParent(Control *parent)
{
    m_pParent = parent;
}

void CShareData::SetCurState(E_STATE s)
{
    m_eLastState = m_eCurState;
    printf("sharedata.cpp  last state is %d\n",m_eLastState);
    m_eCurState = s;
    printf("sharedata.cpp  cur state is %d\n",m_eCurState);
}

void CShareData::SetPowerMsg(bool power_msg) {
    m_bPowerMsg = power_msg;
}

void CShareData::SetEnableMsg(bool enable_msg) {
    m_bEnableMsg = enable_msg;
}

 void CShareData::SetSig(E_SIG sig)
 {
     m_pParent->handleSig(sig);
 }