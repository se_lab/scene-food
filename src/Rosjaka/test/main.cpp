#include "Control.h"
#include "Net/Net.h"
#include <signal.h>
int EXITFLAG = 0;
void mexit(int sig)
{
    printf("running ctrl c\n");
    signal(SIGINT,SIG_DFL);
    EXITFLAG = 1;
}
int main() 
{
    Control C;
    signal(SIGINT,mexit);

    while (1) 
    {
        hv_delay(1000);
        if(EXITFLAG == 1)
        {
            C.sltExit();
        }
    }
    return 0;
}
