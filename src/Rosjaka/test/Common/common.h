//
// Created by 17501 on 2022/1/12.
//

#ifndef MUTIL_JAKA_COMMON_H
#define MUTIL_JAKA_COMMON_H
#define DESCOUNT 4
enum E_STATE
{
    STANDBY,
    BUSY,
    READY,
    RUNNING,
    FINISHED
};

enum E_SIG
{
    SIGPOWER,
    SIGENABLE,
    SIGMKCOF,
    SIGPLACECOF,
    SIGRESET,
    SIGFINMOVE
};

#endif //MUTIL_JAKA_COMMON_H
