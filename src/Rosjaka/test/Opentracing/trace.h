// For opentracing/zipkin
#include "text_map_carrier.h"
#include <cassert>

#include <zipkin/opentracing.h>
using namespace zipkin;
using namespace opentracing;
#include <unordered_map>

# define MAXLINE 1024
enum E_SPAN_REFERENCE
{
    CHILDOF,
    FOLLOWFROM
};
class CTrace
{
public:

    ~CTrace();

    /*********************
     * 入口：msg srv的spanContext
     * 出口：更新后的spanContext
     * 接收span 
     *      callback获取spanContext
     *      生成tracer
     *      设置属性
     *      更新spanContext
     * 发送span
     * ********************/

    static CTrace&  GetInstance();
    /****************************************************TRACE****************************************************/
    /*******************
     * Para: tracer name
     * Description: 生成tracer对象
     * *****************/
    void GenTracer(std::__cxx11::string);
    /*******************
     * Para : tracer name
     * Description: 获取Tracer对象
     * 注：不建议使用该函数直接操作tracer，减少指针持有者
     * *****************/
    std::shared_ptr<opentracing::v3::Tracer> GetTracer(std::string name);


    /****************************************************spanContext****************************************************/

    /*******************
     * Para: name : 自己定义的Context name   context: 由上游节点传下来的spanContext
     * Description: 储存传下来的spanContext
     * 注: 上游spanContext  name格式：xxxCtx   xxx为msg或srv的名称
     * *****************/
    void RecvParentContext(std::string,std::string);
    /*******************
     * Para : context_name 要反序列化的Context的name  tracer_name：反序列化所用到的tracer name
     * Description: 通过tracer进行反序列化，将spanContext由string转为key-value的形式
     * 注：工具函数，需要反序列化时调用。存储的context都是string
     * *****************/
    opentracing::v3::expected<std::unique_ptr<opentracing::v3::SpanContext>>  ExtractContext(std::string context_name,std::string tracer_name);
    /*******************
     * Para: name : tracer_name : span所属tracer的name   span_name:需要生成context的span名  context_name：要生成的新context名
     * Description: 为span生成新的spanContext
     * 注: 新spanContext  name格式：xxxCtx   xxx为对应span的名称
     * *****************/
    void GenNewContext(std::string tracer_name,std::string span_name,std::string context_name);
    /*******************
     * Para : context_name 
     * Description: 获取spanContext
     * 注：向下传输span时使用
     * *****************/
    std::string GetContext(std::string);
    

    /****************************************************Span****************************************************/

    /*******************
     * Para : context_name 上游span的context  tracer_name：生成span所用到的tracer name  span_name: 新span的名称
     *              e_type : 与上游span的关系
     * Description: 生成新span
     * 注：新span  name格式：xxxSp   xxx恰当表示某种功能
     * *****************/
    void StartSpan(std::string tracer_name,std::string span_name,std::string context_name,E_SPAN_REFERENCE);

    void StartSpan(std::string tracer_name,std::string span_name);

    /*******************
    * Para : span_name ：要log的span的name fields：与原log函数参数一致 
     * Description: 为span加log
     * *****************/ 
    void Log(std::string span_name,std::initializer_list<std::pair<string_view, Value>> fields);
    /*******************
     * Para : span_name ：要设置tag的span的name  key: value:与原SetTag参数相同
     * Description: 为span加log
     * *****************/
    void SetTag(std::string span_name,opentracing::v3::string_view key, const opentracing::v3::Value &value);


    /****************************************************Free****************************************************/

    /*******************
     * Para : tracer_name ：要close的tracer name  span_name:要finish的span name context_name：不会再使用的context的name
     * Description: 正确关闭trace span 并清理context
     * 注：重载版本没有清理context
     * *****************/
    void Close(std::string tracer_name,std::string span_name,std::string context_name);
    void Close(std::string tracer_name,std::string span_name);
    /*******************
    *  Para : tracer_name ：要关闭的tracer
     * Description: 关闭tracer
     * *****************/
    void Close(std::string tracer_name);
    /*******************
    * Para : span_name ：要关闭的span
     * Description: 关闭span
     * *****************/
    void Finish(std::string span_name);

    /*******************
     * Para : context_name ：要清理的context名称
     * Description: 清理context
     * 注：使用方式：参数数量任意，适用于集中清理     e.g. ClearContext(name1,name2,name3......)
     * *****************/
    void ClearContext(std::string& context_name);

private:
    CTrace();
    std::unordered_map<std::string,std::string> m_context;
    std::unordered_map<std::string,std::shared_ptr<opentracing::v3::Tracer> > m_tracer;
    std::unordered_map<std::string,std::unique_ptr<opentracing::v3::Span> > m_span;
};