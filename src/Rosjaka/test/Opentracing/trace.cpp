#include "Opentracing/trace.h"

CTrace::CTrace()
{

}

CTrace& CTrace::GetInstance()
{
    //线程安全
    //局部静态对象，c++11引入magic static特性：如果当变量在初始化的时候，并发同时进入声明语句，并发线程将会阻塞等待初始化结束
    static CTrace s_instance;
    return s_instance;
}

CTrace::~CTrace()
{
    //map 收尾
}

void CTrace::GenTracer(std::string service_name) 
{
    // init opentracing zipkin
    ZipkinOtTracerOptions options;
    options.service_name = service_name;
    auto tracer = makeZipkinOtTracer(options);
    assert(tracer);
    m_tracer[service_name] = tracer;
}

void CTrace::StartSpan(std::string tracer_name,std::string span_name,std::string context_name,E_SPAN_REFERENCE e_type)
{
    auto spanContext = ExtractContext(context_name,tracer_name);
    if(m_span.count(span_name))
    {
        m_span.erase(span_name);
    }
    if(e_type == CHILDOF)
    {
        auto span = m_tracer[tracer_name]->StartSpan(span_name, {ChildOf(spanContext->get())});
        // m_span.emplace(span_name,span);
            m_span.insert(std::make_pair(span_name,std::move(span)));
    }
    else if(e_type == FOLLOWFROM)
    {
        auto span = m_tracer[tracer_name]->StartSpan(span_name, {FollowsFrom(spanContext->get())});
        // m_span.emplace(span_name,span);
        m_span.insert(std::make_pair(span_name,std::move(span)));
    }
}

void CTrace::StartSpan(std::string tracer_name,std::string span_name)
{
    auto span = m_tracer[tracer_name]->StartSpan(span_name);
    m_span.insert(std::make_pair(span_name,std::move(span)));
}

std::shared_ptr<opentracing::v3::Tracer> CTrace::GetTracer(std::string name)
{
    if(m_tracer.find(name)!=m_tracer.end())
        return m_tracer[name];
    else
    {
        printf("There is no tracer named \"%s\" ",name.c_str());
        return nullptr;
    }
}

void CTrace::RecvParentContext(std::string name,std::string context)
{
    m_context[name] = context;
}

opentracing::v3::expected<std::unique_ptr<opentracing::v3::SpanContext>>  CTrace::ExtractContext(std::string context_name,std::string tracer_name) {
    char contxt[MAXLINE];
    strcpy(contxt, m_context[context_name].c_str());
    std::unordered_map<std::string, std::string> text_map;
    TextMapCarrier::write_span_context(std::ref(text_map), &contxt[0]);
    TextMapCarrier carrier(text_map);
    auto span_context_maybe = m_tracer[tracer_name]->Extract(carrier); // extraction
    return span_context_maybe;
  }

void CTrace::GenNewContext(std::string tracer_name,std::string span_name,std::string context_name)
{
    std::unordered_map<std::string, std::string> text_map;
    TextMapCarrier carrier(text_map);
    auto err = m_tracer[tracer_name]->Inject(m_span[span_name]->context(), carrier);
    auto new_context = TextMapCarrier::read_span_context(text_map);
    m_context[context_name] = new_context;
}

std::string CTrace::GetContext(std::string name)
{
    if(m_context.find(name)!=m_context.end())
        return m_context[name];
    else
    {
        printf("There is no spanContext named \"%s\" ",name.c_str());
        return "";
    }
}

void CTrace::Log(std::string span_name,std::initializer_list<std::pair<string_view, Value>> fields)
{
    auto itr = m_span.find(span_name);
    if(itr != m_span.end())
    {
        itr->second.get()->Log(fields);
    }
}

void CTrace::SetTag(std::string span_name,opentracing::v3::string_view key, const opentracing::v3::Value &value)
{
    auto itr = m_span.find(span_name);
    if(itr != m_span.end())
    {
        itr->second.get()->SetTag(key,value);
    }
}

void CTrace::Close(std::string tracer_name,std::string span_name,std::string context_name)
{
    Close(tracer_name);
    Finish(span_name);
    ClearContext(context_name);
}

void CTrace::Close(std::string tracer_name,std::string span_name)
{
    Close(tracer_name);
    Finish(span_name);
}

void CTrace::Close(std::string tracer_name)
{
    m_tracer[tracer_name]->Close();
    m_tracer.erase(tracer_name);
}

void CTrace::Finish(std::string span_name)
{
    auto itr = m_span.find(span_name);
    if(itr != m_span.end())
    {
        itr->second.get()->Finish();
        m_span.erase(span_name);
    }
}

void CTrace::ClearContext(std::string& context_name)
{
    m_context.erase(context_name);
}


