import sys

from requests import auth

import math
import socket
import traceback
import time
#import numpy as np

#import rospy
import logging
import requests
class ag95():
    def __init__(self, *args):
        # self.ip = gripper_ip
        self.tcp_server_addr = (args[0], args[1])
        self.gripper_sock = None
        self.connect()


    def connect(self):
        self.gripper_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # keep alive: the time of 'reconnect'; time interval; the count of retry
        self.gripper_sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
        self.gripper_sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 10)
        self.gripper_sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 3)
        self.gripper_sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 5)
        try:
            self.gripper_sock.connect(self.tcp_server_addr)
            logging.info("connect to gripper successfully")
	    print "connect to gripper successfully"
        except socket.error as msg:
            logging.info('connect failed: {}'.format(msg))
	    print "connect to gripper failed"

    def initial_gripper(self):
	print "initial gripper"
        INIT_COMMD = 'FFFEFDFC010802010000000000FB'
        INIT_RECV = 'FFFEFDFC010802010000000000FB'

        self.gripper_sock.sendall(INIT_COMMD.encode())
        time.sleep(3)
        recv_data = self.gripper_sock.recv(2048).decode()
        if recv_data == INIT_RECV:
            logging.info("gripper init success")
        else:
            logging.info('gripper init failure')
    
    def pose_max(self):
        self.pose(target=100)

    def pose_min(self):
        self.pose(target=20)

    # def decrease_in_slow_mode(self):
    #     ...

    # def increase_in_slow_mode(self):
    #     ...

    def pose(self, target, retry=3):
        assert target >= 0 and target <= 100
        for _ in range(retry):
            try:
                hex_target = str(hex(target)[2:4]).upper()
                GRASP_COMMD = 'FFFEFDFC0106020100{}000000FB--(Set Position {})'.format(hex_target, target)
                GRASP_RECV = 'FFFEFDFC0106020100{}000000FB'.format(hex_target)

                self.gripper_sock.sendall(GRASP_COMMD.encode())
                logging.debug(GRASP_COMMD)
                recv_data = self.gripper_sock.recv(2048).decode()
                logging.debug(recv_data)
                time.sleep(3)
                if  GRASP_RECV == recv_data:
                    logging.info('gripper pose success: pose {}'.format(target))
                    break
                else:
                    raise socket.error
            except socket.error as msg:
                logging.info('gripper pose error: '.format(msg))
                self.close()
                # if new_order:
                #     self.initial_gripper()
                self.connect()
                logging.info('retrying ...')

    def close(self):
        # self.gripper_sock.shutdown(socket.SHUT_RDWR)
        self.gripper_sock.close()

if __name__ == '__main__':
    print sys.argv[1]
    if(sys.argv[1] == '1'):
        gripper = ag95('192.168.31.29',8888)
        gripper.initial_gripper() 
	time.sleep(3)
    if(sys.argv[1] == '2'):
        gripper = ag95('192.168.31.29',8888)
        gripper.pose(55)
	time.sleep(1)
    if(sys.argv[1] == '3'):
        gripper = ag95('192.168.31.29',8888)
        gripper.pose(100)
	time.sleep(1)


